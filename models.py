from peewee import *
import datetime
import json

db = SqliteDatabase("data.db", threadlocals=True)

class PostgresqlModel(Model):
   """A base model that will use our Postgresql database"""
   class Meta:
      database = db

class User(PostgresqlModel):
   id = PrimaryKeyField()
   name = CharField(default="")
   admin = BooleanField(default=False)

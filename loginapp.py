from flask import Flask, request, session, redirect, flash, render_template, g, url_for
from flask_admin import Admin
from flask_admin.contrib.peewee import ModelView
from models import *
import requests

app = Flask(__name__)
admin = Admin(app)

class MyModelView(ModelView):
   def is_accessible(self):
      return g.user is not None and g.user.admin

mv = MyModelView(User, db)
mv.name = "User"
admin.add_view(mv)

@app.route('/')
def index():
   return render_template('index.html') 

@app.route('/register', methods=['GET', 'POST'])
def register():
   if request.method == 'POST':      
      use = User.create(name=request.form['username'])
      return redirect(url_for('index'))
   return render_template('register.html')

@app.route('/login')
def login():
   return render_template('login.html')

@app.route('/users')
def users():
   userList = User.select()
   return render_template('userlist.html', users=userList)

#open/close db before/after connection
@app.before_request
def before_request():
   g.db = db
   g.db.connect()

@app.after_request
def after_request(response):
   g.db.close()
   return response

if __name__ == '__main__':
   app.run(debug=True, host="127.0.0.1")
